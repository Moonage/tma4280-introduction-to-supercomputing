#include <cmath>
#include <iostream>
#include <iomanip>
#include <cstdlib>
#include "mpi.h"

using namespace std;

#define THEORETICAL_PI 3.141592653589793238462643383279502


int main(int argc, char **argv)
{

   	int nprocs, rank;
  	MPI_Init(&argc, &argv);

 	  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  	MPI_Comm_size(MPI_COMM_WORLD, &nprocs);
  	MPI_Status stat;

   	double starting_time, finishing_time, error;

   	if (nprocs != 0 && (nprocs & (nprocs-1)))
   	{
   		if (rank==0)		
	    {
	        cout<<"Number is not power of 2. Try again with the correct value."<<endl;
	        MPI_Finalize();
	        return 0;
	    }
   	}


    int N = 16;
    int offset = rank* N / nprocs;
   	int step = N/ nprocs;

    double local_sum = 0.0;
    double computed_PI = 0.0;

    for (int i = offset + 1; i <= offset + step; i++)
    {
        local_sum += 1.0/pow(i,2);
    }

    MPI_Reduce(&local_sum, &computed_PI, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);

    if (rank == 0)
    {
        computed_PI = sqrt(6.0*computed_PI);
    		finishing_time	= MPI_Wtime();
    		cout << "Computed value of PI with Riemann Zeta Method and MPI "<< computed_PI << 
    		". Error is: " << abs(THEORETICAL_PI - computed_PI) <<
    		". Time is: " << finishing_time - starting_time 
    		 << endl;
    }
  
  
  MPI_Finalize();
  return 0;
}
