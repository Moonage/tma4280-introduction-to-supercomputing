#include <cmath>
#include <iostream>
#include <iomanip>
#include <cstdlib>

using namespace std;

#define THEORETICAL_PI 3.141592653589793238462643383279502

int main() {

    cout << "Enter a integer n " << endl;
    int n;
    cin >> n;

    double part_5 = 0.0;
    double part_239 = 0.0;

    clock_t time;
    time = clock();

    #pragma omp parallel for reduction (+: part_5, part_239)

    for(int i= 1;i<n; i++)
    {

        part_5 += pow(-1, i-1)*pow( 0.2,2*i-1) /(2*i-1);
        part_239 += pow(-1,i -1)*pow( 239.0,-(2*i-1)) /(2*i-1);

    }

    double computed_PI = 16.0*part_5 - 4.0*part_239;
    double total_time = ( (float)clock()-time)/CLOCKS_PER_SEC;

    cout << " Computed PI with Machin method is " << setprecision(17) << computed_PI << endl;
    cout <<" The error with " << n <<" steps is " << setprecision(17) <<  abs(THEORETICAL_PI-computed_PI) << endl;
    cout << " total time for calculation was: " << total_time << endl;

    return 0;

}
