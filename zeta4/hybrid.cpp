#include <cmath>
#include <iostream>
#include <iomanip>
#include <cstdlib>

#include "mpi.h"

using namespace std;

#define THEORETICAL_PI 3.141592653589793238462643383279502

int main(int argc, char **argv)
{
    int rank, size;

    MPI_Init(&argc, &argv);

    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);

    MPI_Status status;

    if(size & (size - 1) ){

        if(rank == 0){
            printf("Try again with a power of 2\n");
        }
        MPI_Finalize();
        return 0;

    }

    int n;
    double local_sum = 0.0;
    double starting_time;

    if(rank == 0){

        cout << "Enter a integer n " << endl;
        int n;
        cin >> n;
        starting_time = MPI_Wtime();

    }

    MPI_Bcast(&n, 1,MPI_INT, 0, MPI_COMM_WORLD);

    MPI_Barrier(MPI_COMM_WORLD);

    double sum= 0.0;
    #pragma omp parallel for reduction (+: sum)
    for(int i = rank+1;i<n; i+=size)
    {

        sum += 1.0/pow(i,2);

    }

    local_sum = sum;
    double total_PI = 0.0;

    MPI_Reduce(&local_sum, &total_PI, 2, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);

    if(rank == 0){

        double computed_PI= sqrt(6*total_PI);
        double total_time = MPI_Wtime() - starting_time;

        cout << " Computed PI with ZETA RIEMANN method is " << setprecision(17) << computed_PI << endl;
        cout <<" The error with " << size << " processors and " <<  n <<" steps is " << setprecision(17) <<  abs(THEORETICAL_PI-computed_PI) << endl;
        cout << " total time for calculation was: " << total_time << endl;

    }

    MPI_Finalize();

    return 0;

}
