#include <cmath>
#include <iostream>
#include <iomanip>
#include <cstdlib>
#include "mpi.h"

using namespace std;

#define THEORETICAL_PI 3.141592653589793238462643383279502

int main(int argc, char **argv)
{
	int rank, size, i;
	MPI_Status status;
	MPI_Init(&argc, &argv);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Comm_size(MPI_COMM_WORLD, &size);
	
	int max_N = 16;
	int n = max_N / size;
	double error = 0.0;
	double partial_vector_sum = 0.0;

	// Create a buffer that will hold a subset of the v_i values
	double * vector = (double *) malloc(sizeof(double) * (rank ? n : max_N));

	if (rank == 0) {
		for (int i = 1; i <= max_N; i++) {
			vector[i] = 4.0*pow(-1.0,(i-1))*1.0/(pow(5,(2*i-1))*(2*i-1)) - pow(-1.0,(i-1))*1.0/(pow(239, (2*i-1))*(2*i-1));
		}
	}

	// Scatter the data splits to all processes
	MPI_Scatter(vector, n, MPI_DOUBLE, vector, n, MPI_DOUBLE, 0, MPI_COMM_WORLD);

	// Compute the sum of all elements of our subset
	for(int i = 0; i < n; i++){
		partial_vector_sum += vector[i]; 
	}

	// Gather all partial sums down to the root process

	double total_vector_sum = 0.0;

	// Reduction algorithm for the global sum. The process 0 will be the only one knowing the answer
	MPI_Reduce(&partial_vector_sum, &total_vector_sum, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);

	// Compute final result
	if (rank == 0){
		double computed_pi = 4*total_vector_sum;
		error = abs(computed_pi - THEORETICAL_PI);
		cout << "Computed PI_MACHIN is: " << computed_pi << " with error: " << error  << endl;
	}

	free(vector);
	MPI_Finalize();

	return 0;
}
