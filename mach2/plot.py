# -*- coding: utf-8 -*-
#!/usr/bin/python

import numpy as np
import matplotlib.pyplot as plt

with open("vtest.txt") as f:
    data = f.read()

data = data.split('\n\n')[:-1]

N = [row.split(' ')[0] for row in data]
time = [row.split(' ')[1] for row in data]
error = [row.split(' ')[2] for row in data]


fig = plt.figure()

ax1 = fig.add_subplot(111)

ax1.set_title("Error and timing")    
ax1.set_xlabel('your x label..')
ax1.set_ylabel('your y label...')

ax1.plot(N,error, c='r', label='the data')

leg = ax1.legend()



plt.show()

