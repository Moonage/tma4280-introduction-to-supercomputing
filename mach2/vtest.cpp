#include <cmath>
#include <iostream>
#include <iomanip>
#include <cstdlib>
#include <fstream>
#include "mpi.h"

using namespace std;

#define THEORETICAL_PI 3.141592653589793238462643383279502


int main(int argc, char **argv)
{

    int nprocs, rank;
    MPI_Init(&argc, &argv);

    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &nprocs);
    MPI_Status stat;

    ofstream myfile;
    myfile.open("vtest.txt");

    double error;

    if (nprocs != 0 && (nprocs & (nprocs-1)))
    {
      if (rank==0)    
      {
          cout<<"Number is not power of 2. Try again with the correct value."<<endl;
          MPI_Finalize();
          return 0;
      }
    }

    for (int j=3; j<=15; j++){
      double starting_time;
      if(rank==0){
          double starting_time = MPI_Wtime();
      }

      int N = pow( 2, j );
      int n = N /nprocs;

      int offset = rank * n;
      double local_sum = 0.0;
      for (int i = offset + 1; i <= offset + n; i++)
      {
        local_sum += 4.0 * pow(-1.0, (i - 1) ) * 1.0/ ( pow(5, (2 * i - 1)) * (2 * i - 1))
            - pow(-1.0, (i - 1) ) * 1.0/ (pow(239, (2 * i - 1)) * (2 * i-1)); 
        
      }
      double computed_PI = 0.0;
      MPI_Reduce(&local_sum, &computed_PI, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);

      if (rank == 0)
      {
        computed_PI = 4.0*computed_PI;
        double finishing_time  = MPI_Wtime();
        cout << "Computed value of PI with Machin Method and MPI "<< computed_PI << 
        " for  " << N <<
        ". Error is: " << abs(THEORETICAL_PI - computed_PI) <<
        ". Time is: " << MPI_Wtime() - starting_time 
         << endl;

        myfile <<  N << " " << error << " " <<  MPI_Wtime() - starting_time << "\n" << endl;
      }
    }
  

  MPI_Finalize();
  myfile.close();
  return 0;
}