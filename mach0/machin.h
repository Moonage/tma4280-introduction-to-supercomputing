double machin(int n){
	double result, part_1, part_2 = 0.0;
	int i;

	for (i=1; i<=n; ++i){
		part_1 = pow(-1.0,i-1)*1.0/(pow(5,2*i-1))/(2*i-1);
		part_2 = pow(-1.0,i-1)*1.0/(pow(239,2*i-1))/(2*i-1);

		result += 4*part_1 - part_2;
	}
	return 4*result;
}
