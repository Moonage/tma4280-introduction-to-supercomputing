#include <cmath>
#include <iostream>
#include <iomanip>

#include "machin.h"

using namespace std;

// perform Method 2

int main(){
	
	int number;
	double result = 0.0;
	cout << "Enter an integer " << endl;
	cin >> number;

	result = machin(number);

	cout << "The result with Machin method is: " << setprecision(17) << result << endl;

	return 0;
}