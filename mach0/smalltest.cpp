#include <cmath>
#include <iostream>
#include <iomanip>

#include "machin.h"
using namespace std;

int main() 
{

	int default_n = 3;
	double long expected_result = 3.141592653589793238462643383279502;
	double pi_machin = machin(default_n);

	cout << "Calculated: "<< setprecision(17) << pi_machin << endl;
	cout << "Theoretical: "<< setprecision(17) << expected_result << endl;
	cout << "Error for PI_MACHIN METHOD is: " << setprecision(17) << abs(pi_machin - expected_result) << endl;

	return 0;
}