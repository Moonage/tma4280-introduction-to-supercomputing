#include <cmath>
#include <iostream>
#include <iomanip>
#include <cstdlib>

using namespace std;

#define THEORETICAL_PI 3.141592653589793238462643383279502

int main() {

    cout << "Enter a integer n " << endl;
    int n;
    cin >> n;

    double sum = 0.0;

    clock_t time;
    time = clock();

    #pragma omp parallel for reduction (+: sum)

    for(int i= 1;i<n; i++)
    {
        sum += 1.0/pow(i,2);
    }

    double computed_PI = sqrt(6*sum);
    double total_time = ( (float)clock()-time)/CLOCKS_PER_SEC;

    cout << " Computed PI with ZETA method is " << setprecision(17) << computed_PI << endl;
    cout <<" The error with " << n <<" steps is " << setprecision(17) <<  abs(THEORETICAL_PI-computed_PI) << endl;
    cout << " total time for calculation was: " << total_time << endl;

    return 0;

}
