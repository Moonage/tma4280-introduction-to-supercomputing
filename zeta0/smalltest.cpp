#include <cmath>
#include <iostream>
#include <iomanip>
#include "riemann.h"

using namespace std;

int main() 
{

	double pi_zeta = riemann(3);

	double long expected_result = 3.141592653589793238462643383279502;
	
	// cout << "After comparison on the zeta method, the result is: " << setprecision(17) << (pi_zeta == expected_result) << endl;
	cout << "Calculated: "<< setprecision(17) << pi_zeta << endl;
	cout << "Theoretical: "<< setprecision(17) << expected_result << endl;
	cout << "Error for PI_ZETA METHOD is: " << setprecision(17) << abs(pi_zeta - expected_result) << endl;


	return 0;
}