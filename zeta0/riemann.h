// Riemann header

double riemann(int n){
	double result = 0.0;
	int i;
	for (i=1; i<=n; ++i){
		result += 1.0/pow(i,2);
	}
	return sqrt(result*6.0);
}