#include <cmath>
#include <iostream>
#include <iomanip>

#include "riemann.h"

using namespace std;

// perform Method 1


int main(){
	
	int number;
	cout << "Enter an integer " << endl;
	cin >> number;

	double result = riemann(number);

	cout << "The result with Riemann zeta is: " << setprecision(17) << result << endl;

	return 0;
}