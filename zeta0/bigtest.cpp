// Big test for Riemann

#include <cmath>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <ctime>

#include "riemann.h"

using namespace std;


#define THEORETICAL_PI 3.141592653589793238462643383279502

int main()
{
 	ofstream myfile;
 	myfile.open("vtest.txt");

	int k, curr_n;
	int limit = 24;

	clock_t begin_time = clock();
	
	for (k=1; k <= limit; k++){
		
		int curr_n = pow(2,k);
		double calculated_pi = riemann(curr_n);

		double error = abs(calculated_pi - THEORETICAL_PI);



		myfile << "This is ITERATION with n = " << curr_n << " and the ERROR is: " << setprecision(17) << error
		<< "  WHOLE PROGRAM took so far: " << float( clock () - begin_time ) /  CLOCKS_PER_SEC << endl;

	}
	
	myfile.close();
	return 0;
}