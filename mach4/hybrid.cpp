#include <cmath>
#include <iostream>
#include <iomanip>
#include <cstdlib>

#include "mpi.h"

using namespace std;

#define THEORETICAL_PI 3.141592653589793238462643383279502

int main(int argc, char **argv)
{
    int rank, size;

    MPI_Init(&argc, &argv);

    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);

    MPI_Status status;

    if(size & (size - 1) ){

        if(rank == 0){
            printf("Try again with a power of 2\n");
        }
        MPI_Finalize();
        return 0;

    }

    int n;
    double local_sum[2] = {0,0};
    double starting_time;

    if(rank == 0){

        cout << "Enter a integer n " << endl;
        int n;
        cin >> n;
        starting_time = MPI_Wtime();

    }

    MPI_Bcast(&n, 1,MPI_INT, 0, MPI_COMM_WORLD);

    MPI_Barrier(MPI_COMM_WORLD);

    double part_5 = 0.0;
    double part_239 = 0.0;
    #pragma omp parallel for reduction (+: part_5, part_239)
    for(int i = rank+1;i<n;i+=size)
    {

        part_5 += pow(-1,i-1)* pow(0.2,2*i-1 )/(2*i -1);
        part_239 += pow(-1,i-1)* pow(239.0,-(2*i-1))/ (2*i-1);

    }

    local_sum[0] = part_5;
    local_sum[1] = part_239;

    double total_PI[2] = {0,0};

    MPI_Reduce(&local_sum, &total_PI, 2, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);

    if(rank == 0){

        double computed_PI= 16*total_PI[0] - 4*total_PI[1];
        double total_time = MPI_Wtime() - starting_time;

        cout << " Computed PI with Machin method is " << setprecision(17) << computed_PI << endl;
        cout <<" The error with " << size << " processors and " <<  n <<" steps is " << setprecision(17) <<  abs(THEORETICAL_PI-computed_PI) << endl;
        cout << " total time for calculation was: " << total_time << endl;

    }

    MPI_Finalize();

    return 0;

}
